module ApplicationHelper
  def glyphicon(icon, options = {})
    options.merge! class: "glyphicon #{icon} #{options[:class]}"
    content_tag :span, nil, options
  end

  def awesomeicon(icon, options = {})
    options.merge! class: "pe-7s-#{icon} #{options[:class]}"
    content_tag :i, nil, options
  end
end

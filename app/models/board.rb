class Board < ActiveRecord::Base
include AASM
  aasm  column: 'state' do
    state :waiting, :initial => true
    state :playing
    state :endedplayer
    state :endedcpu
    state :endedtie

    event :start do
      transitions :from => :waiting, :to => :playing
    end

    event :wonplayer do
      transitions :from => :playing, :to => :endedplayer
    end
    event :woncpu do
      transitions :from => :playing, :to => :endedcpu
    end
    event :tie do
      transitions :from => :playing, :to => :endedtie
    end
  end

	def next_move(row, column)
		board= JSON.parse(state_ids)
		self.start if self.waiting?
		if board[row][column].nil?
			board[row][column]= 1
		end
		make_move(board)
	end

private
	def check(board,initial_row, initial_col, add_row, add_col)
			@player1=0
			@cpu=0
			saved_row= 0
			saved_col= 0
			row= initial_row
			column= initial_col
			blocked_play= false
			blocked_cpu= false
			@max.times do
				if board[row].nil? || board[row][column].nil? || (board[row][column] == 0)
					saved_row= row
					saved_col= column
				end
				if !(board[row].nil? || board[row][column].nil?)
					if board[row][column]==1
						@player1= @player1 + 1
						blocked_cpu= true
					end
					if board[row][column]==2
						@cpu= @cpu + 1
						blocked_play= true
					end
				end
				row= row + add_row
				column= column + add_col
			end
			@game_ended= (@player1 == @max) || (@cpu == @max)
			if !@game_ended && (!blocked_play && (@player1 > @max_play) || (!blocked_cpu && (@cpu > @max_play) && ((@player1 < @cpu) )))
				@max_row= saved_row
				@max_col= saved_col
				if !blocked_play && (@player1 > @max_play)
					@max_play= @player1
				elsif !blocked_cpu && (@player1 < @cpu) && (@cpu > @max_play)
					@max_play= @cpu
					@game_won= (@cpu + 1) == @max
				end
			end
	end
	def make_move(board)
		@max_play= 0
		@max_col= 0
		@max_row= 0
		@max= board.size
		column= 0
		@game_ended= false
		@game_won= false
		while !@game_ended && (column < @max)
			check(board,0, column, 1, 0)
			column= column + 1
		end
		row= 0
		while !@game_ended && (row < @max)
			check(board,row, 0, 0, 1)
			row = row + 1
		end
		check(board,0, 0, 1, 1) unless @game_ended

		check(board,0,@max-1, 1, -1)  unless @game_ended

		if  !@game_ended && (( board[@max_row].nil? || board[@max_row][@max_col].nil?) || board[@max_row][@max_col] < 1 )
			if (@max_play == 1) && (( board[@max/2].nil? || board[@max/2][@max/2].nil?) || board[@max/2][@max/2] < 1 )
				board[@max/2][@max/2]= 2
			else
				board[@max_row][@max_col]= 2
			end
		elsif !@game_ended && (board[@max_row][@max_col] >= 1)
			self.tie
		end
		self.woncpu if @game_won
		self.wonplayer if !@game_won && @game_ended
		self.state_ids= board.to_json
		self.save if self.changed?
	end
end

Rails.application.routes.draw do
  resources :boards

  root "static_pages#home", page: "home"
  get 'static_pages/home'
  get 'static_pages/x3'
  post 'static_pages/x3'
  get 'static_pages/tic_tac_toe'
  get 'static_pages/cv'
  get 'static_pages/move'
  get 'static_pages/about'
end

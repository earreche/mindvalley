class StaticPagesController < ApplicationController
	require 'json'

	def cv
		@page_name == 'json_to_html'
		file= File.read('app/assets/javascripts/cv.json')
		@file= JSON.parse(file)

		respond_to do |format|
			format.html
			format.json do
				temp_file = Tempfile.new([' ', '.json'], :encoding => 'ascii-8bit')
					temp_file.write(file)
					temp_file.rewind
					send_file (temp_file), disposition: 'attachment'
				end
		end
	end

	def tic_tac_toe
		@page_name == 'tic_tac_toe'
	end

	def x3
		@page_name == 'tic_tac_toe'
		@max= params[:board_size]
		if !@max.nil?
			Board.delete(session[:current_game])
			session[:current_game]= nil
			@max= @max.to_i
		end
		if (session[:current_game].nil?)
			@board= (Array.new(@max) { Array.new(@max) })
			@b= Board.create(state_ids: @board.to_json, color: "blue")
			session[:current_game]||= @b.id
		else
			@b= Board.where(id:session[:current_game]).first
			if !@b.nil?
				@board= JSON.parse(@b.state_ids)
				size= @board.size
				@max= size
			else
				size= nil
			end
			if size != @max
				@board= (Array.new(@max) { Array.new(@max) })
				@b= Board.create(state_ids: @board.to_json, color: "blue")
				session[:current_game]= @b.id
			end
		end
	end

	def move
		cell= params[:id]
		b= Board.find(session[:current_game])
		b.next_move((cell.to_i%10), (cell.to_i/10))
		@b = b
		@board= JSON.parse(b.state_ids)
		@max= @board.size
		respond_to do |format|
			format.js
		end
	end
end
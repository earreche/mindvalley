// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap-sprockets
//= require bootstrap
//= require theme/bootstrap-checkbox-radio-switch.js
//= require theme/bootstrap-notify.js
//= require theme/bootstrap-select.js
//= require theme/light-bootstrap-dashboard.js
//= require turbolinks

$(document).on('turbolinks:load', function() {
  $('[data-toggle="tooltip"]').tooltip();
  $(".dropdown-toggle").dropdown();
});
$(document).on('turbolinks:load', function() {
	$(".no_turbolinks").click(function(event){
	    event.preventDefault();
	});
});
$(document).on('turbolinks:before-cache', function() {
	navbar_initialized = false;
})
class CreateBoards < ActiveRecord::Migration
  def change
    create_table :boards do |t|
      t.string :state_ids
      t.string :color
      t.timestamps
    end
  end
end
